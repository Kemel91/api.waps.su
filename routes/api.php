<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
Route::middleware('auth:airlock')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::namespace('Api')->group(function () {
    Route::get('/information/{key}', 'InformationController@show');
    Route::get('/cities', 'CityController@index');
    Route::get('/regions', 'RegionController@index');
    Route::get('/regions/{region}', 'RegionController@show');
    Route::get('/companies', 'CompanyController@index');
    Route::post('/companies', 'CompanyController@store');
    Route::get('/companies/{id}', 'CompanyController@show');
    Route::get('/reviews', 'ReviewController@index');
    Route::post('/reviews', 'ReviewController@store');
    Route::group(['prefix' => 'admin','middleware' => 'auth:airlock'], function () {
        Route::get('/dashboard', function (\Illuminate\Http\Request $request) {
            return $request->user();
        });
    });

});
