<?php

use Illuminate\Database\Seeder;

class InformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['rules','about'];
        $data = [];
        foreach ($keys as $key) {
            $dat['key'] = $key;
            $dat['text'] = file_get_contents(__DIR__ . '/'. $key.'.txt');
            $data[] = $dat;
        }
        \DB::table('information')->insert($data);
    }
}
