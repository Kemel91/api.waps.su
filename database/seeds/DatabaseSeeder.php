<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //factory('App\User', 40)->create();
        $this->call(InformationSeeder::class);
        $this->call(CityTableSeeder::class);
    }
}
