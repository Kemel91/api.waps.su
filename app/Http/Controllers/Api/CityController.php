<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CityService;

/**
 * Class CityController
 * @package App\Http\Controllers\Api
 */
class CityController extends Controller
{
    /**
     * @var CityService
     */
    private $service;

    /**
     * CityController constructor.
     * @param CityService $service
     */
    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $cities = $this -> service -> getAllCities();
        return response() -> json(['data' => $cities]);
    }
}
