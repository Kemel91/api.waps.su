<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewStoreRequest;
use App\Services\ReviewService;

/**
 * Class ReviewController
 * @package App\Http\Controllers\Api
 */
class ReviewController extends Controller
{
    /**
     * @var ReviewService
     */
    private $service;

    /**
     * ReviewController constructor.
     * @param ReviewService $service
     */
    public function __construct(ReviewService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $reviews = $this->service->getLastReviews();
        return response()->json($reviews);
    }

    /**
     * @param ReviewStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReviewStoreRequest $request) {
        $data = $request -> validated();
        $review = $this->service->save($data);
        return response()->json($review);
    }
}
