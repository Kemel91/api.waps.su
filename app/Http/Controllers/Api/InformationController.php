<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\InformationService;
use App\Http\Resources\InformationResource;

/**
 * Class InformationController
 * @package App\Http\Controllers\Api
 */
class InformationController extends Controller
{
    /**
     * @var InformationService
     */
    private $service;

    /**
     * InformationController constructor.
     * @param InformationService $service
     */
    public function __construct(InformationService $service)
    {
        $this -> service = $service;
    }

    /**
     * @param string $key
     * @return InformationResource
     */
    public function show(string $key)
    {
        $data = $this -> service -> getByKey($key);
        return new InformationResource($data);
    }
}
