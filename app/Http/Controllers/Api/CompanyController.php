<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyStoreRequest;
use App\Services\CompanyService;
use App\Helpers\ImageUpload;

/**
 * Class CompanyController
 * @package App\Http\Controllers\Api
 */
class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $service;

    /**
     * CompanyController constructor.
     * @param CompanyService $service
     */
    public function __construct(CompanyService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $companies = $this->service->getAllCompanies();
        return response()->json(['data' => $companies]);
    }

    /**
     * Выводим информацию о компании
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) {
        return response()->json(['data' => $this -> service -> getCompany($id)]);
    }

    /**
     * Сохраняем новую компанию
     * @param CompanyStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(CompanyStoreRequest $request) {
        $data = $request -> validated();
        if ($request -> hasFile('file')) {
           $upload = new ImageUpload('companies');
           $data['logo_url'] = $upload -> upload($request -> file('file'));
           $data['logo_preview_url'] = $upload -> makePreview($request -> file('file'),'companies/previews');
        }
        $company = $this -> service -> saveCompany($data);
        return response() -> json($company);
    }
}
