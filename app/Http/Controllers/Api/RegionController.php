<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RegionService;

/**
 * Class RegionController
 * @package App\Http\Controllers\Api
 */
class RegionController extends Controller
{
    /**
     * @var RegionService
     */
    private $service;

    /**
     * RegionController constructor.
     * @param RegionService $service
     */
    public function __construct(RegionService $service)
    {
        $this->service = $service;
    }

    /**
     * Возвращаем список всех регионов
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $regions = $this->service->getAllRegions();
        return response() -> json(['data' => $regions]);
    }

    /**
     * Возвращаем список городов в регионе
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) {
        $cities = $this->service->getRegionCities($id);
        return response() -> json(['data' => $cities]);
    }
}
