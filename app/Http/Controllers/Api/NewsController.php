<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsStoreRequest;
use App\Services\NewsService;

/**
 * Class NewsController
 * @package App\Http\Controllers\Api
 */
class NewsController extends Controller
{
    /**
     * @var NewsService
     */
    private $service;

    /**
     * NewsController constructor.
     * @param NewsService $service
     */
    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        return response()->json(['data' => $this->service->lastNews()]);
    }

    /**
     * @param NewsStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(NewsStoreRequest $request) {
        $data = $request -> validated();
        $save = $this->service->saveNews($data);
        return response()->json(['data' => $save]);
    }
}
