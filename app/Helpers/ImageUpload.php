<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Написал небольшой класс загрузки изображений
 * и превью к ним
 * Using: $image = new ImageUpload('директория', 'имя(не обязательно)','диск(не обязательно)');
 * Загрузка изображения: $file_url = $image -> upload($request -> file);
 * Превью для файла: $file_url_preview = $image -> makePreview('файл(путь)','директория куда сохранять','ширина(320)');
 * Class ImageUpload
 * @package App\Helpers
 * @author Канат Харасаев
 */
class ImageUpload
{
    private $disk;

    private $directory;

    private $name;

    private $ext;

    /**
     * ImageUpload constructor.
     * @param string $directory
     * @param string $name
     * @param string $disk
     * @throws \Exception
     */
    public function __construct(string $directory, $name = '', $disk = 'public')
    {
        $this -> setDisk($disk);
        $this -> setDirectory($directory);
        $this -> setName($name);
    }

    /**
     * Upload file
     * @param $file
     * @return string
     */
    public function upload($file):string {
        $this -> setExt($file -> getClientOriginalExtension());
        return Storage::disk($this->getDisk())->putFileAs($this -> getDirectory(), $file, $this->getFullName());
    }

    /**
     * Создание превью для картинки
     * Работает только после загрузки основной
     * @param $file
     * @param $directory
     * @param int $width
     * @return string
     */
    public function makePreview($file, $directory, $width = 320) {
        $this -> checkDirectory($directory);
        $name = $this -> getFullName();
        $path_file = $directory.'/'.$name;
        //$path_save = 'storage/'.$url_file;
        $path_save = storage_path('app/public/'.$path_file);
        Image::make($file)->resize($width,null,function ($constraint) {
            $constraint->aspectRatio();
        }) -> save($path_save);
        return $path_file;
    }

    /**
     * @param string $disk
     */
    public function setDisk($disk) {
        $disks = ['local','public','s3'];
        if (!in_array($disk,$disks)) $disk = 'public';
        $this -> disk = $disk;
    }

    /**
     * @return string
     */
    public function getDisk() {
        return $this->disk;
    }

    /**
     * Устанавливаем расширение файла
     * @param string $ext
     */
    private function setExt($ext) {
        $this -> ext = $ext;
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    public function setName($name) {
        $this -> name = (empty($name)) ? $this -> getRandName() : $name;
    }
    /**
     * @return string
     */
    private function getFullName() {
        return $this->name.'.'.$this->ext;
    }

    /**
     * @param string $name
     */
    public function setDirectory($name) {
        $this -> checkDirectory($name);
        $this -> directory = $name;
    }

    /**
     * Если не существует, создаем директорию
     * @param $name
     * @return bool
     */
    public function checkDirectory($name) {
        if (Storage::disk($this->getDisk())->exists($name) === false) {
            Storage::disk($this->getDisk())->makeDirectory($name);
        }
        return true;
    }

    /**
     * @return string
     */
    public function getDirectory() {
        return $this -> directory;
    }


    /**
     * Выводим рандомное название
     * @param int $length
     * @return string
     * @throws \Exception
     */
    private function getRandName($length = 8) {
        return bin2hex(random_bytes($length));
    }
}
