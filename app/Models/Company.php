<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'id',
        'name',
        'city_id',
        'address',
        'email',
        'site',
        'logo_url',
        'logo_preview_url',
        'description',
        'created_at',
        'updated_at'
    ];

    public function city() {
        return $this -> belongsTo(City::class);
    }

    public function reviews() {
        return $this -> hasMany(Review::class);
    }

    public function getLogoUrlAttribute($value) {
        if (empty($value)) return 'https://ipsumimage.appspot.com/360x250';
        return secure_asset('storage/'.$value);
    }

    public function getLogoPreviewUrlAttribute($value) {
        if (empty($value)) return 'https://ipsumimage.appspot.com/80x80';
        return secure_asset('storage/'.$value);
    }
}
