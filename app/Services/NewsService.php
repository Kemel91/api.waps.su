<?php


namespace App\Services;

use App\Models\News;

/**
 * Class NewsService
 * @package App\Services
 */
class NewsService
{
    /**
     * @return mixed
     */
    public function lastNews() {
        return News::orderBy('id','desc')->take(5)->get();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function saveNews(array $data) {
        $new = new News($data);
        return $new -> save();
    }
}
