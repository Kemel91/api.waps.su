<?php
namespace App\Services;

use App\Models\Review;

/**
 * Class ReviewService
 * @package App\Services
 */
class ReviewService
{
    /**
     * Последние 6 комментариев
     * @return \Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getLastReviews() {
        return Review::with('company:id,name,logo_preview_url')
                    -> orderBy('id','desc')
                    -> take(6)
                    -> get();
    }
    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data) {
        $model = new Review($data);
        return $model -> save();
    }
}
